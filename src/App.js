import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Nav, Navbar, NavItem } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import "./App.css";
import Routes from "./Routes";

function App(props) {

  return (
    <div className="App container">
      <Navbar collapseOnSelect>
        <Navbar>
          <Navbar.Brand>
            <Link to="/">MARA</Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar>
        <Navbar.Collapse>
          <Nav className="float-right">
            <LinkContainer to="/signup">
              <NavItem>Signup</NavItem>
            </LinkContainer>
            <LinkContainer to="/login">
              <NavItem>Login</NavItem>
            </LinkContainer>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <Routes />
    </div>
  );
}

export default App;