import React from "react";
import "./Home.css";

export default function Home() {
  return (
    <div className="Home">
      <div className="lander">
        <h1>Homepage</h1>
        <p>My amazing Homepage</p>
      </div>
    </div>
  );
}