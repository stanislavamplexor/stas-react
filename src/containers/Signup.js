import React, { useState } from "react";
import { Button, FormGroup, FormControl, FormLabel } from "react-bootstrap";
import "./Signup.css";

export default function Signup() {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  function validateForm() {

    return username.length > 0 && email.length > 0 && password.length;
  }

  function handleSubmit(event) {
    alert('Te-ai inregistrat! Fake, dar te-ai inregistrat!');
  }

  return (
    <div className="Signup">
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="username">
          <FormLabel>Username*</FormLabel>
          <FormControl
            autoFocus
            type="textfield"
            value={username}
            onChange={e => setUsername(e.target.value)}
            required
          />
        </FormGroup>
        <FormGroup controlId="email">
          <FormLabel>Email*</FormLabel>
          <FormControl
            type="email"
            value={email}
            onChange={e => setEmail(e.target.value)}
            required
          />
        </FormGroup>
        <FormGroup controlId="password">
          <FormLabel>Password*</FormLabel>
          <FormControl
            value={password}
            onChange={e => setPassword(e.target.value)}
            type="password"
            required
          />
        </FormGroup>
        <Button block disabled={!validateForm()} type="submit">
          Signup
        </Button>
      </form>
    </div>
  );
}